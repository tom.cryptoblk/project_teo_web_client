import { Component, Input } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'working-location-item',
  templateUrl: './working-location-item.component.html',
  styleUrls: ['./working-location-item.component.scss']
})
export class WorkingLocationItemComponent {
  @Input() name: string;
  faTimes = faTimes;

  level = 1;
}
