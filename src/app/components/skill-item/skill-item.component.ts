import { Component, OnInit, Input } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'skill-item',
  templateUrl: './skill-item.component.html',
  styleUrls: ['./skill-item.component.scss']
})
export class SkillItemComponent implements OnInit {
  @Input() name: string;
  faTimes = faTimes;

  level = 1;

  constructor() {}

  ngOnInit() {}

  onRate(e) {
    if (e && e.newValue) {
      this.level = e.newValue;
    }
  }
}
