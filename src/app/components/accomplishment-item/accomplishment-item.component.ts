import { Component, OnInit, Input } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'accomplishment-item',
  templateUrl: './accomplishment-item.component.html',
  styleUrls: ['./accomplishment-item.component.scss']
})
export class AccomplishmentItemComponent {
  @Input() name: string;
  faTimes = faTimes;
}
