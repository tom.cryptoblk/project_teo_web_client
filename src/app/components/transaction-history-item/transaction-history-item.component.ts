import { Component, Input } from '@angular/core';

import { Transaction } from '../../interfaces/transaction-interface'

@Component({
  selector: 'transaction-history-item',
  templateUrl: './transaction-history-item.component.html',
  styleUrls: ['./transaction-history-item.component.scss']
})
export class TransactionHistoryItemComponent {
  @Input() tx: Transaction;
}
