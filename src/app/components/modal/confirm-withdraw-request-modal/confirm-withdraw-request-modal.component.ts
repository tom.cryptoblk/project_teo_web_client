import { Component, Input, Output, EventEmitter } from '@angular/core';
import { faMailBulk } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'confirm-withdraw-request-modal',
  templateUrl: './confirm-withdraw-request-modal.component.html',
  styleUrls: ['./confirm-withdraw-request-modal.component.scss']
})

export class ConfirmWithdrawRequestModalComponent {
  @Input() visible: boolean;

  @Output() onClose: EventEmitter<any> = new EventEmitter();

  faMailBulk = faMailBulk;


  closeModal() {
    this.onClose.emit();
  }
}
