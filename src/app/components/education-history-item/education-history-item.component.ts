import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'education-history-item',
  templateUrl: './education-history-item.component.html',
  styleUrls: ['./education-history-item.component.scss']
})
export class EducationHistoryItemComponent implements OnInit {
  // options
  years: Array<number> = [];
  months: Array<string> = [
    'January',
    'February	',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    for (let i = 1980; i <= currentYear; i++) {
      this.years.push(i);
    }
    this.years.reverse();
  }
}
