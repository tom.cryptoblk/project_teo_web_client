import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'qualification-item',
  templateUrl: './qualification-item.component.html',
  styleUrls: ['./qualification-item.component.scss']
})
export class QualificationItemComponent implements OnInit {
  // options
  years: Array<number> = [];

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    for (let i = 1980; i <= currentYear; i++) {
      this.years.push(i);
    }
    this.years.reverse();
  }
}
