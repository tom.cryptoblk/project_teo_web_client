import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'withdraw-form',
  templateUrl: './withdraw-form.component.html',
  styleUrls: ['./withdraw-form.component.scss']
})
export class WithdrawFormComponent implements OnInit {
  @Output() onFinishForm: EventEmitter<any> = new EventEmitter();

  amount: number = null;

  constructor() { }

  ngOnInit() { }

  onSubmit(event: Event, form: NgForm) {
    event.preventDefault();
    const amount: number = form.value.amount;
    const validation = form.valid;

    if (validation) {
      // do someting here
      this.onFinishForm.emit({ amount });
    }
  }
}
