import { Component, OnInit, ViewChild } from '@angular/core';
import { faDollarSign, faPlus, faChevronLeft, faCloudUploadAlt, faCalendar } from '@fortawesome/free-solid-svg-icons';
import { NgForm, FormControl, FormBuilder } from '@angular/forms';
import swal from 'sweetalert2'

import { ExpertBasicProfileServiceService } from '../../../../services/expert/expert-basic-profile-service.service'
import { AccountService } from '../../../../services/account.service'

@Component({
  selector: 'expert-basic-profile-form',
  templateUrl: './expert-basic-profile-form.component.html',
  styleUrls: ['./expert-basic-profile-form.component.scss']
})

export class ExpertBasicProfileFormComponent implements OnInit {

  constructor(
    private expertBasicProfileServiceService: ExpertBasicProfileServiceService,
    private accountService: AccountService
  ) { }

  userId = null;
  selected = new FormControl(0);

  faCloudUploadAlt = faCloudUploadAlt;
  faDollarSign = faDollarSign;
  faPlus = faPlus;
  faChevronLeft = faChevronLeft;
  faCalendar = faCalendar;

  // options
  years: Array<number> = [];
  months: Array<number> = [];

  englishLevel = 0;

  englishLevels: Array<{ name?: string }> = [
    { name: `I don't know english :(` },
    { name: 'Beginner/Elementary' },
    { name: 'Pre-Intermediate' },
    { name: 'Upper Intermediate' },
    { name: 'Advanced/Fluent' }
  ];

  employmentOptions: Array<{ id: string; name?: string }> = [
    { id: '0', name: `Full time` },
    { id: '1', name: 'Remote work' },
    { id: '2', name: 'One time project' },
    { id: '3', name: 'Relocate' }
  ];

  skillInput = '';
  accomplishmentInput = '';
  workingLocationInput = '';

  skills: Array<{ name?: string; level?: number }> = [];
  accomplishments: Array<{ name?: string; verified?: boolean }> = [];
  workingLocations: Array<{ name?: string }> = [];

  employments: Array<string> = [];

  workingExperiences: Array<{
    companyName?: string;
    title?: string;
    start?: number;
    end?: number;
  }> = [{ companyName: '' }];

  educationHistories: Array<{
    collageName?: string;
    degree?: string;
    start?: number;
    end?: number;
  }> = [{ collageName: '' }];

  qualifications: Array<{
    organization?: string;
    description?: string;
    year?: number;
  }> = [{ organization: '' }];

  industries: Array<string> = [
    'Accounting',
    'Airlines/Aviation',
    'Alternative Dispute Resolution',
    'Alternative Medicine',
    'Animation',
    'Apparel & Fashion',
    'Architecture & Planning',
    'Arts & Crafts',
    'Automotive',
    'Aviation & Aerospace',
    'Banking',
    'Biotechnology',
    'Broadcast Media',
    'Building Materials',
    'Business Supplies & Equipment',
    'Capital Markets',
    'Chemicals',
    'Civic & Social Organization',
    'Civil Engineering',
    'Commercial Real Estate',
    'Computer & Network Security',
    'Computer Games',
    'Computer Hardware',
    'Computer Networking',
    'Computer Software',
    'Construction',
    'Consumer Electronics',
    'Consumer Goods',
    'Consumer Services',
    'Cosmetics',
    'Dairy',
    'Defense & Space',
    'Design',
    'Education Management',
    'E-learning',
    'Electrical & Electronic Manufacturing',
    'Entertainment',
    'Environmental Services',
    'Events Services',
    'Executive Office',
    'Facilities Services',
    'Farming',
    'Financial Services',
    'Fine Art',
    'Fishery',
    'Food & Beverages',
    'Food Production',
    'Fundraising',
    'Furniture',
    'Gambling & Casinos',
    'Glass, Ceramics & Concrete',
    'Government Administration',
    'Government Relations',
    'Graphic Design',
    'Health, Wellness & Fitness',
    'Higher Education',
    'Hospital & Health Care',
    'Hospitality',
    'Human Resources',
    'Import & Export',
    'Individual & Family Services',
    'Industrial Automation',
    'Information Services',
    'Information Technology & Services',
    'Insurance',
    'International Affairs',
    'International Trade & Development',
    'Internet',
    'Investment Banking/Venture',
    'Investment Management',
    'Judiciary',
    'Law Enforcement',
    'Law Practice',
    'Legal Services',
    'Legislative Office',
    'Leisure & Travel',
    'Libraries',
    'Logistics & Supply Chain',
    'Luxury Goods & Jewelry',
    'Machinery',
    'Management Consulting',
    'Maritime',
    'Marketing & Advertising',
    'Market Research',
    'Mechanical or Industrial Engineering',
    'Media Production',
    'Medical Device',
    'Medical Practice',
    'Mental Health Care',
    'Military',
    'Mining & Metals',
    'Motion Pictures & Film',
    'Museums & Institutions',
    'Music',
    'Nanotechnology',
    'Newspapers',
    'Nonprofit Organization Management',
    'Oil & Energy',
    'Online Publishing',
    'Outsourcing/Offshoring',
    'Package/Freight Delivery',
    'Packaging & Containers',
    'Paper & Forest Products',
    'Performing Arts',
    'Pharmaceuticals',
    'Philanthropy',
    'Photography',
    'Plastics',
    'Political Organization',
    'Primary/Secondary',
    'Printing',
    'Professional Training',
    'Program Development',
    'Public Policy',
    'Public Relations',
    'Public Safety',
    'Publishing',
    'Railroad Manufacture',
    'Ranching',
    'Real Estate',
    'Recreational',
    'Facilities & Services',
    'Religious Institutions',
    'Renewables & Environment',
    'Research',
    'Restaurants',
    'Retail',
    'Security & Investigations',
    'Semiconductors',
    'Shipbuilding',
    'Sporting Goods',
    'Sports',
    'Staffing & Recruiting',
    'Supermarkets',
    'Telecommunications',
    'Textiles',
    'Think Tanks',
    'Tobacco',
    'Translation & Localization',
    'Transportation/Trucking/Railroad',
    'Utilities',
    'Venture Capital',
    'Veterinary',
    'Warehousing',
    'Wholesale',
    'Wine & Spirits',
    'Wireless',
    'Writing & Editing'
  ];

  countries: Array<string> = [
    'Afghanistan',
    'Albania',
    'Algeria',
    'Andorra',
    'Angola',
    'Anguilla',
    'Antigua &amp; Barbuda',
    'Argentina',
    'Armenia',
    'Aruba',
    'Australia',
    'Austria',
    'Azerbaijan',
    'Bahamas',
    'Bahrain',
    'Bangladesh',
    'Barbados',
    'Belarus',
    'Belgium',
    'Belize',
    'Benin',
    'Bermuda',
    'Bhutan',
    'Bolivia',
    'Bosnia &amp; Herzegovina',
    'Botswana',
    'Brazil',
    'British Virgin Islands',
    'Brunei',
    'Bulgaria',
    'Burkina Faso',
    'Burundi',
    'Cambodia',
    'Cameroon',
    'Canada',
    'Cape Verde',
    'Cayman Islands',
    'Chad',
    'Chile',
    'China',
    'Colombia',
    'Congo',
    'Cook Islands',
    'Costa Rica',
    'Cote D Ivoire',
    'Croatia',
    'Cruise Ship',
    'Cuba',
    'Cyprus',
    'Czech Republic',
    'Denmark',
    'Djibouti',
    'Dominica',
    'Dominican Republic',
    'Ecuador',
    'Egypt',
    'El Salvador',
    'Equatorial Guinea',
    'Estonia',
    'Ethiopia',
    'Falkland Islands',
    'Faroe Islands',
    'Fiji',
    'Finland',
    'France',
    'French Polynesia',
    'French West Indies',
    'Gabon',
    'Gambia',
    'Georgia',
    'Germany',
    'Ghana',
    'Gibraltar',
    'Greece',
    'Greenland',
    'Grenada',
    'Guam',
    'Guatemala',
    'Guernsey',
    'Guinea',
    'Guinea Bissau',
    'Guyana',
    'Haiti',
    'Honduras',
    'Hong Kong',
    'Hungary',
    'Iceland',
    'India',
    'Indonesia',
    'Iran',
    'Iraq',
    'Ireland',
    'Isle of Man',
    'Israel',
    'Italy',
    'Jamaica',
    'Japan',
    'Jersey',
    'Jordan',
    'Kazakhstan',
    'Kenya',
    'Kuwait',
    'Kyrgyz Republic',
    'Laos',
    'Latvia',
    'Lebanon',
    'Lesotho',
    'Liberia',
    'Libya',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macau',
    'Macedonia',
    'Madagascar',
    'Malawi',
    'Malaysia',
    'Maldives',
    'Mali',
    'Malta',
    'Mauritania',
    'Mauritius',
    'Mexico',
    'Moldova',
    'Monaco',
    'Mongolia',
    'Montenegro',
    'Montserrat',
    'Morocco',
    'Mozambique',
    'Namibia',
    'Nepal',
    'Netherlands',
    'Netherlands Antilles',
    'New Caledonia',
    'New Zealand',
    'Nicaragua',
    'Niger',
    'Nigeria',
    'Norway',
    'Oman',
    'Pakistan',
    'Palestine',
    'Panama',
    'Papua New Guinea',
    'Paraguay',
    'Peru',
    'Philippines',
    'Poland',
    'Portugal',
    'Puerto Rico',
    'Qatar',
    'Reunion',
    'Romania',
    'Russia',
    'Rwanda',
    'Saint Pierre &amp; Miquelon',
    'Samoa',
    'San Marino',
    'Satellite',
    'Saudi Arabia',
    'Senegal',
    'Serbia',
    'Seychelles',
    'Sierra Leone',
    'Singapore',
    'Slovakia',
    'Slovenia',
    'South Africa',
    'South Korea',
    'Spain',
    'Sri Lanka',
    'St Kitts &amp; Nevis',
    'St Lucia',
    'St Vincent',
    'St. Lucia',
    'Sudan',
    'Suriname',
    'Swaziland',
    'Sweden',
    'Switzerland',
    'Syria',
    'Taiwan',
    'Tajikistan',
    'Tanzania',
    'Thailand',
    "Timor L'Este",
    'Togo',
    'Tonga',
    'Trinidad &amp; Tobago',
    'Tunisia',
    'Turkey',
    'Turkmenistan',
    'Turks &amp; Caicos',
    'Uganda',
    'Ukraine',
    'United Arab Emirates',
    'United Kingdom',
    'United States',
    'United States Minor Outlying Islands',
    'Uruguay',
    'Uzbekistan',
    'Venezuela',
    'Vietnam',
    'Virgin Islands (US)',
    'Yemen',
    'Zambia',
    'Zimbabwe'
  ];

  profileId = null;
  profile = null;

  @ViewChild('form') basicProfileForm: NgForm;

  addSkill() {
    const { skillInput } = this;
    if (skillInput && skillInput.length > 0) {
      this.skills.push({
        name: this.skillInput,
        level: 0
      });
      this.skillInput = '';
    }
  }

  addAccomplishment() {
    const { accomplishmentInput } = this;
    if (accomplishmentInput && accomplishmentInput.length > 0) {
      this.accomplishments.push({
        name: this.accomplishmentInput,
        verified: false
      });
      this.accomplishmentInput = '';
    }
  }

  addWorkingLocation() {
    const { workingLocationInput } = this;
    if (workingLocationInput && workingLocationInput.length > 0) {
      this.workingLocations.push({
        name: workingLocationInput
      });
      this.workingLocationInput = '';
    }
  }

  async onSubmit(event: Event, form: NgForm) {
    try {
      event.preventDefault();
      const {
        interests,
        selfIntroduction,
        minimalSalary,
        desiredPosition,
        industry
      } = form.value
      const editedProfile = {
        ...this.profile,
        industry,
        interests,
        selfIntroduction,
        minimalSalary,
        desiredPosition
      }

      await this.expertBasicProfileServiceService.update(editedProfile)
      this.fetchProfile()
      swal.fire({
        type: 'success',
        title: 'Success',
        text: 'Basic profile updated.'
      })
    } catch (error) {
      swal.fire({
        type: 'error',
        title: 'Error',
        text: error.message
      })
    }
  }

  selectEnglishLevel(index) {
    this.englishLevel = index;
  }

  selectEmployment(id) {
    if (this.employments.includes(id)) {
      this.employments = this.employments.filter(e => e !== id);
    } else {
      this.employments.push(id);
    }
  }

  addWorkingExperience() {
    this.workingExperiences.push({ companyName: '' });
  }

  addEducationHistory() {
    this.educationHistories.push({ collageName: '' });
  }

  addQualification() {
    this.qualifications.push({ organization: '' });
  }

  changeTab(index) {
    this.selected.setValue(index);
  }

  async fetchProfile() {
    try {
      const user: any = await this.accountService.identity()
      const { id: userId } = user
      const profile: any = await this.expertBasicProfileServiceService.getProfile(userId)
      if (profile && profile.id) {
        const {
          interests,
          selfIntroduction,
          minimalSalary,
          desiredPosition,
          id,
          industry
        } = profile
        this.profileId = id
        this.profile = profile
        console.log(profile, 'profile...')
        this.basicProfileForm.setValue({
          industry,
          interests,
          selfIntroduction,
          minimalSalary,
          desiredPosition,
          skillInput: this.skillInput,
          accomplishmentInput: this.accomplishmentInput
        })
      }
    } catch (error) {
      console.log(error, 'error')
    }
  }

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    for (let i = 1980; i <= currentYear; i++) {
      this.years.push(i);
    }
    this.fetchProfile()
  }



}
