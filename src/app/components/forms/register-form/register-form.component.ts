import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { faBuilding, faUser, faPaperPlane } from '@fortawesome/free-solid-svg-icons';
import swal from 'sweetalert2'

import { AuthService } from '../../../services/auth.service'
interface RegisterPayload {
  email: string,
  login: string,
  password: string,
  langKey?: string
}

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent {

  constructor(private authService: AuthService, private router: Router) { }

  faBuilding = faBuilding;
  faUser = faUser;

  userType = 'company';
  success = false;
  email = 'nest@ped.com';
  faPaperPlane = faPaperPlane;

  accept = false;

  selectUserType(userType: string) {
    this.userType = userType;
  }

  toggleAccept() {
    this.accept = !this.accept;
  }


  async onSubmit(form: NgForm) {
    try {
      const { email, password, confirm_password } = form.value
      if (password !== confirm_password) {
        throw new Error('Password not match');
      }
      const payload: RegisterPayload = {
        email,
        password,
        login: email,
        langKey: 'en'
      };

      this.email = form.value.email;
      await this.authService.register(payload)
      this.router.navigate(['/auth/email-verify'])
    } catch (err) {
      swal.fire({
        type: 'error',
        title: 'Error',
        text: err.message
      })
    }
  }
}
