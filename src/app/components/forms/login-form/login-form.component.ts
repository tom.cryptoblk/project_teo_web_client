import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../services/auth.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  username = '';
  password = '';

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() { }

  async onSubmit(event: Event, form: NgForm) {
    try {
      event.preventDefault();
      const username: string = form.value.username;
      const password: string = form.value.password;
      const validation = form.valid;
      const rememberMe = true
      if (validation) {
        await this.authService.login({ username, password, rememberMe })
        this.router.navigate(['/']);
      }
    } catch (error) {
      Swal.fire({
        type: 'error',
        title: 'Error',
        text: error.message
      })
    }
  }

}
