import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'working-experience-item',
  templateUrl: './working-experience-item.component.html',
  styleUrls: ['./working-experience-item.component.scss']
})
export class WorkingExperienceItemComponent implements OnInit {
  // options
  years: Array<number> = [];
  months: Array<string> = [
    'January',
    'February	',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  ngOnInit() {
    const currentYear = new Date().getFullYear();
    for (let i = 1980; i <= currentYear; i++) {
      this.years.push(i);
    }
    this.years.reverse();
  }
}
