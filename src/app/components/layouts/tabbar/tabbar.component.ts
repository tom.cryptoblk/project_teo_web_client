import { Component } from '@angular/core';
import { Router, RouterEvent } from '@angular/router';

@Component({
  selector: 'app-tabbar',
  templateUrl: './tabbar.component.html',
  styleUrls: ['./tabbar.component.scss']
})
export class TabbarComponent {
  public section = '';

  constructor(private router: Router) {
    this.router.events.subscribe((event: RouterEvent) => {
      const url = event.url;
      switch (url) {
        case '/mobile/contact/list':
        case '/mobile/contact/message':
          this.section = 'messages';
          break;
        case '/mobile/notifications':
          this.section = 'notifications';
          break;
        case '/mobile/wallet':
          this.section = 'wallet';
          break;
        case '/mobile/profile':
          this.section = 'profile';
          break;
      }
    });
  }

}
