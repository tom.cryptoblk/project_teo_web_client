import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent, ActivatedRoute, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './app-navbar.component.html',
  styleUrls: ['./app-navbar.component.scss']
})
export class AppNavbarComponent implements OnInit {

  public section = '';

  constructor(private router: Router, private route: ActivatedRoute) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event instanceof NavigationEnd) {
        const url = event.url;
        this.section = this.mappingSection(url);
      }
    });
  }

  ngOnInit() {
    const url: string = this.router.url.split('?')[0];
    this.section = this.mappingSection(url);
  }

  mappingSection(url: string): string {
    switch (url) {
      case '/desktop/messages':
        return 'messages';
      case '/desktop/notifications':
        return 'notifications';
      case '/desktop/wallet':
        return 'wallet';
      case '/desktop/profile':
        return 'profile';
    }
    return url;
  }
}
