import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// configure
import { UserRouteAccessService } from './services/user-route-access.service'

// layout
import { AppLayoutComponent } from './components/layouts/app-layout/app-layout.component'
// auth
import { LoginComponent } from './pages/auth/login/login.component'
import { VerifyEmailPageComponent } from './pages/auth/verify-email-page/verify-email-page.component'

//terms
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component'
import { TermOfServicePageComponent } from './pages/term-of-service-page/term-of-service-page.component'
import { CookiePageComponent } from './pages/cookie-page/cookie-page.component'

// app
import { WalletPageComponent } from './pages/wallet-page/wallet-page.component'
import { DepositPageComponent } from './pages/deposit-page/deposit-page.component'
import { WithdrawPageComponent } from './pages/withdraw-page/withdraw-page.component'
import { WithdrawSuccessPageComponent } from './pages/withdraw-success-page/withdraw-success-page.component'
import { EditProfilePageComponent } from './pages/edit-profile-page/edit-profile-page.component'

const routes: Routes = [
  {
    path: 'app',
    canActivate: [UserRouteAccessService],
    component: AppLayoutComponent,
    data: {
      authorities: ['ROLE_USER']
    },
    children: [
      { path: '', component: WalletPageComponent, pathMatch: 'full' },
      { path: 'deposit', component: DepositPageComponent },
      { path: 'withdraw', component: WithdrawPageComponent, pathMatch: 'full' },
      { path: 'withdraw/success', component: WithdrawSuccessPageComponent },
      { path: 'profile/edit', component: EditProfilePageComponent }
    ]
  },
  {
    path: 'auth',
    children: [
      { path: 'login', component: LoginComponent, pathMatch: 'full' },
      { path: 'register', component: LoginComponent, pathMatch: 'full' },
      { path: 'email-verify', component: VerifyEmailPageComponent, pathMatch: 'full' },
    ]
  },
  { path: 'privacy-policy', component: PrivacyPageComponent, pathMatch: 'full' },
  { path: 'cookie-policy', component: CookiePageComponent, pathMatch: 'full' },
  { path: 'term-of-service-policy', component: TermOfServicePageComponent, pathMatch: 'full' },
  { path: '**', redirectTo: '/app' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
