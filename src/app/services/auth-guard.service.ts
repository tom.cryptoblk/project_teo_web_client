import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from './auth.service';

@Injectable()

export class AuthGuardService implements CanActivate {
  constructor(public auth: AuthService, public router: Router) { }

  canActivate(): boolean {
    const isLogin = !this.auth.isAuthenticated()
    if (isLogin) {
      this.router.navigate(['/auth/login']);
      return false;
    }
    return true;
  }
}