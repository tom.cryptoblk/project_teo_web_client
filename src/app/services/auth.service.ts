import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { AccountService } from './account.service';
import { AuthServerProvider } from './auth-jwt.service';
import { API_URL } from '../app.constants'

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private accountService: AccountService,
    private authServerProvider: AuthServerProvider,
    private httpClient: HttpClient
  ) { }

  public isAuthenticated(): boolean {
    const token = this.authServerProvider.getToken()
    const isExpired = token ? false : true
    return isExpired
  }

  login(credentials, callback?) {
    const cb = callback || function () { };

    return new Promise((resolve, reject) => {
      this.authServerProvider.login(credentials).subscribe(
        data => {
          this.accountService.identity(true).then(account => {
            resolve(data);
          });
          return cb();
        },
        err => {
          this.logout();
          reject(err);
          return cb(err);
        }
      );
    });
  }

  register(body) {
    return new Promise((resolve, reject) => {
      this.httpClient.post(API_URL + '/api/register', body).subscribe(
        resp => {
          resolve(resp)
        },
        err => {
          reject(err)
        }
      )
    })
  }

  loginWithToken(jwt, rememberMe) {
    return this.authServerProvider.loginWithToken(jwt, rememberMe);
  }

  logout() {
    this.authServerProvider.logout().subscribe(null, null, () => this.accountService.authenticate(null));
  }

}
