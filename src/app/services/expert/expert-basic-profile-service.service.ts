import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { API_URL } from '../../app.constants'

@Injectable({
  providedIn: 'root'
})
export class ExpertBasicProfileServiceService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getProfile(userId: number) {
    return new Promise((resolve, reject) => {
      this.httpClient.get(API_URL + `/api/expert-info-basics?userId.equals=${userId}`).subscribe(
        result => {
          resolve(result[0])
        },
        err => {
          reject(err)
        }
      )
    })
  }

  update(body: any) {
    return new Promise((resolve, reject) => {
      this.httpClient.put(API_URL + `/api/expert-info-basics`, body).subscribe(
        result => {
          resolve(result)
        },
        err => {
          reject(err)
        }
      )
    })
  }

}
