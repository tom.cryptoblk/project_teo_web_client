import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-wallet-page',
  templateUrl: './wallet-page.component.html',
  styleUrls: ['./wallet-page.component.scss']
})
export class WalletPageComponent implements OnInit {
  transactions = [
    {
      from: '0xF439B8ssse',
      to: '0xF439B8edddss',
      amount: 100,
      createdAt: new Date()
    },
    {
      from: '0xF439B8ssse',
      to: '0xF439B8edddss',
      amount: 112,
      createdAt: new Date()
    },
    {
      from: '0xF439B8ssse',
      to: '0xF439B8edddss',
      amount: 53,
      createdAt: new Date()
    },
    {
      from: '0xF439B8ssse',
      to: '0xF439B8edddss',
      amount: 23,
      createdAt: new Date()
    }
  ];

  constructor() { }

  ngOnInit() {
  }

}
