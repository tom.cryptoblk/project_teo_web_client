import { Component, OnInit } from '@angular/core';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-verify-email-page',
  templateUrl: './verify-email-page.component.html',
  styleUrls: ['./verify-email-page.component.scss']
})
export class VerifyEmailPageComponent implements OnInit {

  faCheckCircle = faCheckCircle;

  success = false;

  constructor() { }

  ngOnInit() {
    setTimeout(() => {
      this.success = true;
    }, 3000);
  }

}
