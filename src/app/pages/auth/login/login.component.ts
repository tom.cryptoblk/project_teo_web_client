import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  isLogin = true;
  isRegister = false;

  constructor(private router: Router) {
    this.router.events.subscribe(() => {
      if (this.router.url === '/auth/login') {
        this.isLogin = true;
        this.isRegister = false;
      } else if (this.router.url === '/auth/register') {
        this.isLogin = false;
        this.isRegister = true;
      }
    });
  }

  ngOnInit() { }

}
