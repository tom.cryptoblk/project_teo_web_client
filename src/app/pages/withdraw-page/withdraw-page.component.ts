import { Component } from '@angular/core';

@Component({
  selector: 'app-withdraw-page',
  templateUrl: './withdraw-page.component.html',
  styleUrls: ['./withdraw-page.component.scss']
})
export class WithdrawPageComponent {
  confirmWithdrawRequestVisible = false;
  histories = [
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 2343
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 10
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 10
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 674
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 554
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 113
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 423
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 221
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 101
    },
    {
      status: 'Completed',
      coin: 'TEO',
      createdAt: new Date(),
      amount: 103
    }
  ];

  public onSubmitForm = () => {
    this.confirmWithdrawRequestVisible = true;
  };

  public closeConfirmWithdrawRequestModal = () => {
    this.confirmWithdrawRequestVisible = false;
  };
}
