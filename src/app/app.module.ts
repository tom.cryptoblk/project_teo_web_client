import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatTabsModule, MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HttpClientModule } from '@angular/common/http';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { QRCodeModule } from 'angular2-qrcode';
import { StarRatingComponent } from 'ng-starrating';

import { AuthInterceptor } from './blocks/interceptor/auth.interceptor'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// pages - auth
import { LoginComponent } from './pages/auth/login/login.component';
import { VerifyEmailPageComponent } from './pages/auth/verify-email-page/verify-email-page.component';

// pages
import { WalletPageComponent } from './pages/wallet-page/wallet-page.component';

// terms
import { PrivacyPageComponent } from './pages/privacy-page/privacy-page.component';
import { CookiePageComponent } from './pages/cookie-page/cookie-page.component';
import { TermOfServicePageComponent } from './pages/term-of-service-page/term-of-service-page.component';

// components
import { LoginFormComponent } from './components/forms/login-form/login-form.component';
import { RegisterFormComponent } from './components/forms/register-form/register-form.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { TransactionHistoryItemComponent } from './components/transaction-history-item/transaction-history-item.component';
import { DepositPageComponent } from './pages/deposit-page/deposit-page.component';
import { WithdrawPageComponent } from './pages/withdraw-page/withdraw-page.component';
import { WithdrawSuccessPageComponent } from './pages/withdraw-success-page/withdraw-success-page.component';
import { EditProfilePageComponent } from './pages/edit-profile-page/edit-profile-page.component';
import { ConfirmWithdrawRequestModalComponent } from './components/modal/confirm-withdraw-request-modal/confirm-withdraw-request-modal.component';
import { WithdrawFormComponent } from './components/forms/withdraw-form/withdraw-form.component';
import { AppLayoutComponent } from './components/layouts/app-layout/app-layout.component';
import { AppNavbarComponent } from './components/layouts/app-navbar/app-navbar.component';
import { TabbarComponent } from './components/layouts/tabbar/tabbar.component'

// edit-profile-components
import { SkillItemComponent } from './components/skill-item/skill-item.component';
import { AccomplishmentItemComponent } from './components/accomplishment-item/accomplishment-item.component';
import { WorkingLocationItemComponent } from './components/working-location-item/working-location-item.component';
import { WorkingExperienceItemComponent } from './components/working-experience-item/working-experience-item.component';
import { EducationHistoryItemComponent } from './components/education-history-item/education-history-item.component';
import { QualificationItemComponent } from './components/qualification-item/qualification-item.component';
import { ExpertBasicProfileFormComponent } from './components/forms/edit-expert-profile/expert-basic-profile-form/expert-basic-profile-form.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LoginFormComponent,
    RegisterFormComponent,
    VerifyEmailPageComponent,
    WalletPageComponent,
    PrivacyPageComponent,
    CookiePageComponent,
    TermOfServicePageComponent,
    SpinnerComponent,
    TransactionHistoryItemComponent,
    DepositPageComponent,
    WithdrawPageComponent,
    WithdrawSuccessPageComponent,
    EditProfilePageComponent,
    ConfirmWithdrawRequestModalComponent,
    WithdrawFormComponent,
    AppLayoutComponent,
    AppNavbarComponent,
    TabbarComponent,
    AccomplishmentItemComponent,
    WorkingLocationItemComponent,
    WorkingExperienceItemComponent,
    EducationHistoryItemComponent,
    QualificationItemComponent,
    SkillItemComponent,
    StarRatingComponent,
    ExpertBasicProfileFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    HttpClientModule,
    NgxWebstorageModule.forRoot(),
    QRCodeModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})

export class AppModule {

}
