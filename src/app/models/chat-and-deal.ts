import { Chat } from './chat';
export interface ChatAndDeal {
  chatDTO: Chat;
  dealDTO: any;
  remark: string;
}
