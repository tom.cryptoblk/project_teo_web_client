export interface Chat {
  chatReceiver: number;
  chatSender: number;
  chatStatus: string;
  chatType: string;
  createdDateTime: string;
  dealId: number;
  filePath: string;
  id: number;
  textContent: string;
  truncate?: string;
}
