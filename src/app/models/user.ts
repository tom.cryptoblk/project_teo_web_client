export interface User {
  activated: boolean;
  authorities: string[];
  createdBy: string;
  createdDate: string;
  email: string;
  firstName: string;
  id: number;
  imageUrl: string;
  langKey: string;
  lastModifiedBy: string;
  lastModifiedDate: string;
  lastName: string;
  login: string;
  letterAvatar?: string;
}
